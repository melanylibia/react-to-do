import {useEffect,useState} from 'react';
import './App.css';
import Form from './components/Form';
import TodoList from './components/TodoList';
function App() {
  const [inputText,setInputText] = useState("")
  const [todos, setTodos] = useState([])
  const [status, setStatus] = useState("all")
  const [filteredTodos,setFilteredTodos] = useState([])
  useEffect(()=>{
    getLocalTodos();
  },[]) 
  const filterHandler = () => {
    switch (status)
    {
      case "uncompleted":
        setFilteredTodos(todos.filter((todo)=> !todo.completed))
        break
      case "completed":
        setFilteredTodos(todos.filter((todo)=> todo.completed))
        break
      default:
        setFilteredTodos(todos)
        break;
    }
  }
  const saveLocalTodos=()=>{
    localStorage.setItem("todos",JSON.stringify(todos));
    
  }
  const getLocalTodos = () =>{
    if(localStorage.getItem("todos")===null){
      localStorage.setItem("todos", JSON.stringify([]))
    }
    else{
      let todoLocal = JSON.parse(localStorage.getItem("todos"))
      setTodos(todoLocal)
    }
  }
  useEffect(()=>{filterHandler();saveLocalTodos();},[todos,status])
  return (
    <div className="App">
      <Form setInputText={setInputText}
        todos={todos}
        setTodos={setTodos}
        inputText={inputText}
        status={status}
        setStatus={setStatus}
      />
      <TodoList todos={todos}
          setTodos={setTodos}
          filteredTodos={filteredTodos}
      />
    </div>
  );
}

export default App;
